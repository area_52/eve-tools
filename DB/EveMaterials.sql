USE master
GO

DROP DATABASE IF EXISTS EVE 
GO

CREATE DATABASE EVE
GO

USE EVE
GO

CREATE SCHEMA EVE
GO

CREATE TABLE EVE.Materials
(
  id INT PRIMARY KEY,
  basePrice MONEY DEFAULT 0.00,
  groupID INT,
  iconID INT,
  materialName VARCHAR(254),
  materialDescription TEXT,
  mass FLOAT,
  portionSize INT,
  published BIT,
  volume DECIMAL(18,3)
)
GO

CREATE TABLE EVE.Compositions
(
  id INT IDENTITY(1,1) PRIMARY KEY,
  parent INT FOREIGN KEY REFERENCES EVE.Materials(id)
		ON DELETE CASCADE,
  component INT FOREIGN KEY REFERENCES EVE.Materials(id),
  quantity BIGINT,
  CONSTRAINT uc_parent_component UNIQUE (parent, component)
)
GO


------------------------------------------------------------ PROCEDIMIENTOS ALMACENADOS

---------------------------- CRUD TABLA EVE.Materiales

-- REGISTRAR Materiales
CREATE PROC RegisterMaterials
  @id INT,
  @basePrice MONEY,
  @groupID INT,
  @iconID INT,
  @materialName VARCHAR(254),
  @materialDescription TEXT,
  @mass FLOAT,
  @portionSize INT,
  @published BIT,
  @volume DECIMAL(18,3)
AS
INSERT INTO EVE.Materials
  (id, basePrice, groupID, iconID, materialName, materialDescription, mass, portionSize, published, volume)
VALUES
  (@id, @basePrice, @groupID, @iconID, @materialName, @materialDescription, @mass, @portionSize, @published, @volume)
GO

-- MODIFICAR Materiales
CREATE PROC UpdateMaterials
  @id INT,
  @basePrice MONEY,
  @groupID INT,
  @iconID INT,
  @materialName VARCHAR(254),
  @materialDescription TEXT,
  @mass FLOAT,
  @portionSize INT,
  @published BIT,
  @volume DECIMAL(18,3)
AS
UPDATE EVE.Materials
  SET
    basePrice = @basePrice,
    groupID = @groupID,
    iconID = @iconID,
    materialName = @materialName,
    materialDescription = @materialDescription,
    mass = @mass,
    portionSize = @portionSize,
    published = @published,
    volume = @volume
  WHERE id = @id
GO

-- LISTAR Materiales
CREATE PROC ListMaterials
AS
SELECT id, basePrice, groupID, iconID, materialName, materialDescription, mass, portionSize, published, volume
FROM EVE.Materials;
GO

-- BUSCAR Material (por nombre)
CREATE PROC SearchMaterialByMaterialName
  @materialName VARCHAR(254)
AS
SELECT id, basePrice, groupID, iconID, materialName, materialDescription, mass, portionSize, published, volume
FROM EVE.Materials
WHERE materialName LIKE '%' + @materialName + '%'
GO

-- ELIMINAR Material
CREATE PROC DeleteMaterial
  @id INT
AS
DELETE FROM EVE.Materials
  WHERE id = @id
GO



---------------------------- CRUD TABLA EVE.Compositions

-- REGISTRAR Composición
CREATE PROC RegisterComposition
  @id INT,
  @parent INT,
  @component INT,
  @quantity BIGINT
AS
INSERT INTO EVE.Compositions
  (id, parent, component, quantity)
VALUES
  (@id, @parent, @component, @quantity)
GO

-- ACTUALIZAR Composición
CREATE PROC UpdateComposition
  @id INT,
  @parent INT,
  @component INT,
  @quantity BIGINT
AS
UPDATE EVE.Compositions
  SET parent = @parent, component = @component, quantity = @quantity
  WHERE id = @id;
GO

-- LISTAR Composición
CREATE PROC ListComposition
AS
SELECT id, parent, component, quantity
FROM EVE.Compositions
GO

-- ELIMINAR Composición
CREATE PROC RemoveComposition
  @id INT
AS
DELETE FROM EVE.Compositions
  WHERE id = @id
GO 
